# -*- encoding: utf-8 -*-

import os, sys, re
import ovh
import swiftclient
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-r', dest="region_name", help="Region", type=str)
parser.add_argument('-c', dest="conf", help="config file", nargs=1, required=True)
args = parser.parse_args()

# OVH client
ovh_client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# region
if args.region_name:
    REGION_NAME = args.region_name
else:
    REGION_NAME = os.getenv('OS_REGION_NAME')

# check
if REGION_NAME == '' or not REGION_NAME:
    sys.exit('Region ?')

if PROJECT_ID == '' or not PROJECT_ID:
    sys.exit('Project ID ?')

# OpenStack connexion
swift_conn = swiftclient.Connection(
        authurl=os.getenv('OS_AUTH_URL'),
        user=os.getenv('OS_USERNAME'),
        key=os.getenv('OS_PASSWORD'),
        os_options={
            'user_domain_name': os.getenv('OS_USER_DOMAIN_NAME'),
            'project_domain_name': os.getenv('OS_PROJECT_DOMAIN_NAME'),
            'project_id': PROJECT_ID,
            'region_name': REGION_NAME},
        auth_version='3'
    )

# def
def container_user(acl):
    users = []
    xcr = acl.split(',')
    for aright in xcr:
        m = re.search('([^:]*):([^:]*)', aright)
        if m:
            projet = m.group(1)
            username = m.group(2)

            for user in ovh_client.get('/cloud/project/%s/user' % PROJECT_ID):
                if username == user['username']:
                    users.append(user)

    return ",".join([str(user['id']) + '/' + user['description'] for user in users])

def get_user(obj, username):

    if not args.username or (args.username == obj['username']):
        user_table.append([
            obj['id'],
            obj['username'],
            obj['creationDate'],
            obj['description'],
            obj['status'],
            ",".join([r['description'] for r in obj['roles']])
            ])

def get_storage(container_name):

    storage = swift_conn.head_container(container=container_name)

    ret = []
    ret.append(container_name)
    ret.append(storage['x-container-object-count'])
    ret.append(storage['x-container-bytes-used'])
    ret.append(storage['x-storage-policy'])
    if 'x-container-read' in storage:
        ret.append(container_user(storage['x-container-read']))
    else:
        ret.append('')
    if 'x-container-write' in storage:
        ret.append(container_user(storage['x-container-write']))
    else:
        ret.append('')
    ret.append(storage['last-modified'])
    storage_table.append(ret)

# print
storage_table = []
user_table = []
for storage in swift_conn.get_account()[1]:
    get_storage(storage['name'])

headers = []
headers.append('Name')
headers.append('Stored Objects')
headers.append('Stored Bytes')
headers.append('Storage policy')
headers.append('Read users')
headers.append('Write users')
headers.append('Last modified')

print (tabulate(storage_table, headers=headers))
