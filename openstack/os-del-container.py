# -*- encoding: utf-8 -*-

import os, sys
import swiftclient
import argparse

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-r', dest="region_name", help="Region", type=str)
parser.add_argument("storagename", help="Storage name", type=str)
args = parser.parse_args()

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# region
if args.region_name:
    REGION_NAME = args.region_name
else:
    REGION_NAME = os.getenv('OS_REGION_NAME')

# check
if REGION_NAME == '' or not REGION_NAME:
    sys.exit('Region ?')

if PROJECT_ID == '' or not PROJECT_ID:
    sys.exit('Project ID ?')

# OpenStack connexion
swift_conn = swiftclient.Connection(
        authurl=os.getenv('OS_AUTH_URL'),
        user=os.getenv('OS_USERNAME'),
        key=os.getenv('OS_PASSWORD'),
        os_options={
            'user_domain_name': os.getenv('OS_USER_DOMAIN_NAME'),
            'project_domain_name': os.getenv('OS_PROJECT_DOMAIN_NAME'),
            'project_id': PROJECT_ID,
            'region_name': REGION_NAME},
        auth_version='3'
    )

# def
def del_storage(container_name):
    swift_conn.delete_container(container=container_name)

# main
for storage in swift_conn.get_account()[1]:
    if storage['name'] ==  args.storagename:
        del_storage(storage['name'])
