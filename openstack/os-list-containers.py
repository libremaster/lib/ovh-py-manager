# -*- encoding: utf-8 -*-

import os, sys
import swiftclient
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-r', dest="region_name", help="Region", type=str)
args = parser.parse_args()

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# region
if args.region_name:
    REGION_NAME = args.region_name
else:
    REGION_NAME = os.getenv('OS_REGION_NAME')

# check
if REGION_NAME == '' or not REGION_NAME:
    sys.exit('Region ?')

if PROJECT_ID == '' or not PROJECT_ID:
    sys.exit('Project ID ?')

# connexion
swift_conn = swiftclient.Connection(
        authurl=os.getenv('OS_AUTH_URL'),
        user=os.getenv('OS_USERNAME'),
        key=os.getenv('OS_PASSWORD'),
        os_options={
            'user_domain_name': os.getenv('OS_USER_DOMAIN_NAME'),
            'project_domain_name': os.getenv('OS_PROJECT_DOMAIN_NAME'),
            'project_id': PROJECT_ID,
            'region_name': REGION_NAME},
        auth_version='3'
    )

# def
def get_storage(container_name):

    storage = swift_conn.head_container(container=container_name)
    ret = []
    ret.append(container_name)
    ret.append(storage['x-container-object-count'])
    ret.append(storage['x-container-bytes-used'])
    ret.append(storage['x-storage-policy'])
    ret.append(storage['x-container-read'])
    if 'x-container-write' in storage:
        ret.append(storage['x-container-write'])
    else:
        ret.append('')
    ret.append(storage['last-modified'])

    table.append(ret)

# print
table = []
headers = []
for storage in swift_conn.get_account()[1]:
    get_storage(storage['name'])

headers.append('Name')
headers.append('Stored Objects')
headers.append('Stored Bytes')
headers.append('Storage policy')
headers.append('Read ACL')
headers.append('Write ACL')
headers.append('Last modified')

print (tabulate(table, headers=headers))
