# -*- encoding: utf-8 -*-

import ovh
import argparse
from tabulate import tabulate
import datetime
import iso8601
import pytz

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-c', dest="conf", help="config file", nargs=1)
parser.add_argument('-e', dest="expiration", help="Expired tokens", action="store_true")
parser.add_argument('-d', dest="delete", help="Delete selection", action="store_true")
parser.add_argument('-dr', dest="display_rules", help="Display rules?", action='store_true')
parser.add_argument('-dtc', dest="display_token_creation", help="Display token creation?", action='store_true')
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# main
credentials = client.get('/me/api/credential', status='validated')

if args.expiration:
    date_now=pytz.utc
    print ('Expired tokens:')

# pretty print credentials status
def table_add_line(credential_id, application, credential, rules, head = True):
    if head:
        line = [
            credential_id,
            '[%s] %s' % (application['status'], application['name']),
            application['description'],
            application['applicationKey']
        ]
    else:
        line = [
            '',
            '',
            '',
            ''
        ]

    if args.display_rules:
        if len(rules) > 0:
            line.append(rules[0]['path'] + ' ' + rules[0]['method'])
        if args.display_token_creation:
            if head:
                line.append(credential['creation'])
            else:
                line.append('')
    else:
        if head:
            line.append(credential['creation'])
        else:
            line.append('')

    if head:
        line += [
            credential['expiration'],
            credential['lastUse']
        ]
    else:
        line += [
            '',
            ''
        ]

    table.append(line)
    if len(rules) > 1 and args.display_rules:
        rules.pop(0)
        table_add_line(credential_id, application, credential, rules, head = False)

table = []
for credential_id in credentials:
    credential_method = '/me/api/credential/'+str(credential_id)
    credential = client.get(credential_method)
    application = client.get(credential_method+'/application')

    process=False
    if args.expiration:
        if credential['expiration'] != None:
            if iso8601.parse_date(credential['expiration']) < datetime.datetime.now(date_now):
                process=True
    else:
        process=True

    if process:
        table_add_line(credential_id, application, credential, credential['rules'])

        if args.delete:
            print ('Effacement du token : ' + str(credential_id))
            delete = client.delete('/me/api/credential/'+str(credential_id))

headers = ['ID', 'App Name', 'Description', 'Application Key']
if args.display_rules:
    headers.append('Rules')
    if args.display_token_creation:
        headers.append('Token Creation')
else:
    headers.append('Token Creation')
headers += ['Token Expiration', 'Token Last Use']
print (tabulate(table, headers=headers))
