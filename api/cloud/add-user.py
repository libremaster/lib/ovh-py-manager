# -*- encoding: utf-8 -*-

import os
import ovh
import argparse
from tabulate import tabulate
from enum import Enum

# parameters
class Role(Enum):
    admin = 'admin'
    authentication = 'authentication'
    administrator = 'administrator'
    compute_operator = 'compute_operator'
    infrastructure_supervisor = 'infrastructure_supervisor'
    network_security_operator = 'network_security_operator'
    network_security = 'network_security'
    backup_operator = 'backup_operator'
    image_operator = 'image_operator'
    volume_operator = 'volume_operator'
    objectstore_operator = 'objectstore_operator'

    def __str__(self):
        return self.value

parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-d', dest="description", help="Description", type=str, required=True)
parser.add_argument('-r', dest="roles", help="Roles", type=Role, choices=list(Role), nargs='*', action='append')
parser.add_argument('-c', dest="conf", help="config file", nargs='?', required=True)
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# def
def get_obj(obj):

    table.append([
        obj['id'],
        obj['username'],
        obj['password'],
        obj['creationDate'],
        obj['description'],
        obj['status'],
        ",".join([r['description'] for r in obj['roles']])
        ])

# main
body_data = {}
body_data['description'] = args.description
body_data['roles'] = []
if args.roles:
    for role in args.roles:
        body_data['roles'].append(str(role[0]))
user = client.post('/cloud/project/%s/user' % PROJECT_ID, **body_data)

# print
table = []
get_obj(user)

print (tabulate(table, headers=['ID', 'Username', 'Password', 'Creation date', 'Description', 'Status', 'Roles']))
