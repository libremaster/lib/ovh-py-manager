# -*- encoding: utf-8 -*-

import ovh
import argparse
from enum import Enum

# parameters
class Section(Enum):
    storage = 'storage'
    user = 'user'

    def __str__(self):
        return self.value

parser = argparse.ArgumentParser()
parser.add_argument('-c', dest="conf", help="config file", nargs=1)
parser.add_argument('-w', dest="newconf", help="new config file", nargs='?')
parser.add_argument('-ro', dest="readonly", help="read only token", action="store_true")
parser.add_argument('-s', dest="sections", help="Sections", type=Section, choices=list(Section), nargs='*', action='append')
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

ck = client.new_consumer_key_request()

if args.sections and len(args.sections) > 0:
    if args.project_id == '' or not args.project_id:
        sys.exit('Project ID ?')

    for section in args.sections:
        # Request /cloud API access
        if args.readonly:
            ck.add_rules(ovh.API_READ_ONLY, "/cloud/project/%s/%s" % (args.project_id, str(section[0])))
            ck.add_rules(ovh.API_READ_ONLY, "/cloud/project/%s/%s/*" % (args.project_id, str(section[0])))
        else:
            ck.add_rules(ovh.API_READ_WRITE, "/cloud/project/%s/%s" % (args.project_id, str(section[0])))
            ck.add_rules(ovh.API_READ_WRITE, "/cloud/project/%s/%s/*" % (args.project_id, str(section[0])))
else:
    # Request /cloud API access    
    if args.readonly:
        ck.add_rules(ovh.API_READ_ONLY, "/cloud")
        ck.add_rules(ovh.API_READ_ONLY, "/cloud/*")
    else:
        ck.add_rules(ovh.API_READ_WRITE, "/cloud")
        ck.add_rules(ovh.API_READ_WRITE, "/cloud/*")

# Request token
validation = ck.request()

print("Visite %s pour t'authentifier et valider l'autorisation d'accès." % validation['validationUrl'])
input("et presse Entrée pour continuer...")

# Print nice welcome message
print("Ta clé d'autorisation d'accès 'consumerKey' est '%s'" % validation['consumerKey'])

# Writing new config file
if args.newconf:
    with open(args.newconf, "w") as newconf:
        newconf.write('[default]\n')
        newconf.write('endpoint=ovh-eu\n\n')
        newconf.write('[ovh-eu]\n')
        newconf.write('application_key=' + client._application_key + '\n')
        newconf.write('application_secret=' + client._application_secret + '\n\n')
        if args.readonly:
            ar_txt = 'Lecture seulement'
        else:
            ar_txt = 'Lecture/Ecriture'
        if args.sections and len(args.sections) > 0:
            for section in args.sections:
                newconf.write('; ' + ar_txt + " /cloud/project/%s/%s" % (args.project_id, str(section[0])) + '\n')
        else:
            newconf.write('; ' + ar_txt + ' /cloud' + '\n')
        newconf.write('consumer_key=' + validation['consumerKey'] + '\n')