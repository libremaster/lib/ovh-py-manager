# -*- encoding: utf-8 -*-

import os
import ovh
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-c', dest="conf", help="config file", nargs='?')
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# def
def get_obj(obj):

    table.append([
        obj['id'],
        obj['name'],
        obj['publicKey'],
        ",".join(obj['regions'])
        ])

# main
objs = client.get('/cloud/project/%s/sshkey' % PROJECT_ID)

# print
table = []
for obj in objs:
    get_obj(obj)

print (tabulate(table, headers=['ID', 'Name', 'public Key', 'Regions']))
