# -*- encoding: utf-8 -*-

import os
import ovh
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument("user_id", help="User ID", type=str)
parser.add_argument('-c', dest="conf", help="config file", nargs=1, required=True)
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# check
if PROJECT_ID == '' or not PROJECT_ID:
    sys.exit('Project ID ?')

# def
def get_obj(obj):

    table.append([
        obj['username'],
        obj['password'],
        obj['status']
        ])

# main
user = client.post('/cloud/project/%s/user/%s/regeneratePassword' % (PROJECT_ID, args.user_id))

# print
table = []
get_obj(user)

print (tabulate(table, headers=['Username', 'Password', 'Status']))
