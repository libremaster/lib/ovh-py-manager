# -*- encoding: utf-8 -*-

import os
import ovh
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-u', dest="username", help="Username", type=str)
parser.add_argument('-c', dest="conf", help="config file", nargs='?')
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# def
def get_obj(obj):

    if not args.username or (args.username == obj['username']):
        table.append([
            obj['id'],
            obj['username'],
            obj['creationDate'],
            obj['description'],
            obj['status'],
            ",".join([r['description'] for r in obj['roles']])
            ])

# main
objs = client.get('/cloud/project/%s/user' % PROJECT_ID)

# print
table = []
for obj in objs:
    get_obj(obj)

print (tabulate(table, headers=['ID', 'Username', 'Creation date', 'Description', 'Status', 'Roles']))
