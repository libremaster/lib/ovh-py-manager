# -*- encoding: utf-8 -*-

import os
import ovh
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-c', dest="conf", help="config file", nargs='?')
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# main
regions = client.get('/cloud/project/%s/region' % PROJECT_ID)

# print
print (",".join(regions))
