# -*- encoding: utf-8 -*-

import ovh
import argparse

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-c', dest="conf", help="config file", nargs='?')
parser.add_argument('-ro', dest="readonly", help="read only token", action="store_true")
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)


# Request RW, /cloud API access
ck = client.new_consumer_key_request()
if args.readonly:
    ck.add_rules(ovh.API_READ_ONLY, "/cloud")
    ck.add_rules(ovh.API_READ_ONLY, "/cloud/*")
    ck.add_rules(ovh.API_READ_ONLY, "/vps")
    ck.add_rules(ovh.API_READ_ONLY, "/vps/*")
else:
    ck.add_rules(ovh.API_READ_WRITE, "/cloud")
    ck.add_rules(ovh.API_READ_WRITE, "/cloud/*")
    ck.add_rules(ovh.API_READ_WRITE, "/vps")
    ck.add_rules(ovh.API_READ_WRITE, "/vps/*")

# Request token
validation = ck.request()

print("Visite %s pour t'authentifier et valider l'autorisation d'accès." % validation['validationUrl'])
input("et presse Entrée pour continuer...")

# Print nice welcome message
print("Ta clé d'autorisation d'accès 'consumerKey' est '%s'" % validation['consumerKey'])
